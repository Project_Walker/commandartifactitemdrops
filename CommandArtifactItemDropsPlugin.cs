﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using CommandArtifactItemDrops.Hooks;
using R2API;
using R2API.Utils;
using Random = System.Random;

namespace CommandArtifactItemDrops
{
    /// <summary>
    /// The Command Artifact Item Drops plugin mod for Risk of Rain 2
    /// </summary>
    [BepInDependency(R2API.R2API.PluginGUID)]
    [R2APISubmoduleDependency(nameof(ItemAPI), nameof(ItemDropAPI), nameof(ResourcesAPI))]
    [BepInPlugin(ModGuid, ModName, ModVer)]
    [NetworkCompatibility(CompatibilityLevel.NoNeedForSync)]
    public class CommandArtifactItemDropsPlugin : BaseUnityPlugin
    {
        private const string ModGuid = "com.maucer.CommandArtifactItemDrops";
        private const string ModName = "Command Artifact Item Drops";
        private const string ModVer = "0.3.1";

        private const float DefaultChanceFromChests = 0.10f;
        private const float DefaultChanceFromSacrificeArtifact = 0.05f;
        private const float DefaultChanceFromDeathRewards = 0.005f;

        internal static float DefaultChanceFromDeathRewardsForUncommon = 0.30f;
        internal static float DefaultChanceFromDeathRewardsForRare = 0.05f;

        /// <summary>
        /// The internal logging source for this plugin
        /// </summary>
        internal new static ManualLogSource Logger;

        /// <summary>
        /// The random number generator used for determining whether a drop chance is successful
        /// </summary>
        internal static Random Random;

        /// <summary>
        /// The time in milliseconds that the Command Artifact will be enabled during a successful drop
        /// </summary>
        internal static int CommandArtifactTimeout = 1000;

        /// <summary>
        /// The configured drop chance percentage for a single item picker drop from
        /// a chest object
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageFromChests { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a single item picker drop from
        /// killing a monster while the sacrifice artifact is enabled
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageFromSacrificeArtifact { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a single item picker drop from
        /// killing a monster from death rewards while the sacrifice artifact is enabled
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageFromDeathRewards { get; set; }

        public void Awake()
        {
            Logger = base.Logger;
            Random = new Random();

            LoadConfigurationSettings(Config);
            InitializeEventHooks();
        }

        /// <summary>
        /// Loads the configuration settings 
        /// </summary>
        /// <param name="config"></param>
        private static void LoadConfigurationSettings(ConfigFile config)
        {
            // get the configured drop chance from chests
            DropChancePercentageFromChests = config.Bind(
                "Chests",
                "Drop Chance Percentage",
                DefaultChanceFromChests,
                $"Sets the chance that a command artifact item picker will drop from chests. [0-100, default={DefaultChanceFromChests:#.00}]");

            // get the configured drop chance from the sacrifice artifact
            DropChancePercentageFromSacrificeArtifact = config.Bind(
                "Sacrifice Artifact",
                "Drop Upgrade Chance Percentage",
                DefaultChanceFromSacrificeArtifact,
                $"Sets the chance that the drop from a slain monster will upgrade to a command artifact item picker when the sacrifice artifact is enabled and a drop occurs. [0-100, default={DefaultChanceFromSacrificeArtifact:#.00}]");

            // get the configured drop chance from death rewards when sacrifice is enabled
            DropChancePercentageFromDeathRewards = config.Bind(
                "Sacrifice Artifact",
                "Drop Chance Percentage from Kill Rewards",
                DefaultChanceFromDeathRewards,
                $"Sets the chance that a command artifact item picker will drop from slain monsters as death rewards when the sacrifice artifact is enabled. [0-100, default={DefaultChanceFromDeathRewards:#.00}]");
        }

        /// <summary>
        /// Initializes the event hooks
        /// </summary>
        private static void InitializeEventHooks()
        {
            ChestBehaviorHooks.Bind();
            SacrificeArtifactManagerHooks.Bind();
            //DeathRewardHooks.Bind();
        }
    }
}
